﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo2
{
    public abstract class Avenger
    {
        Weapon weapon;

        public abstract void Talk();

        public void equipWeapon(Weapon weapon)
        {
            this.weapon = weapon;
        }

        public void Attack()
        {
            weapon.Attack();
        }
    }
}
