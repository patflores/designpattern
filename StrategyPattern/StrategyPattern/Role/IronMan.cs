﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public class IronMan : Avenger
    {
        public override void Talk()
        {
            Console.WriteLine("我是鋼鐵人！");
        }
    }
}
