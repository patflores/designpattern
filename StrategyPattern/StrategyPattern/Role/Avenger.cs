﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public abstract class Avenger
    {
        IAttack weapon;

        public abstract void Talk();

        public void equipWeapon(IAttack weapon)
        {
            this.weapon = weapon;
        }

        public void Attack()
        {
            weapon.Attack();
        }
    }
}
