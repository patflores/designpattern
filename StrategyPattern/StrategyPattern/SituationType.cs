﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public enum SituationType
    {
        OuterSpace = 1,
        Air = 2,
        Land = 3
    }
}
