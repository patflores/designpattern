﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StrategyPattern
{
    public class Shield : Weapon.Weapon, IAttack
    {
        public void Attack()
        {
            Console.WriteLine("使用盾牌攻擊");
        }
    }
}
