﻿using System;

namespace StrategyPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            SituationType situationType = SituationType.Air;

            // 根據不同的情況, 呼叫英雄 (工廠模式)
            Avenger avenger = HeroCenter.callHero(situationType);
            avenger.Talk();


            // 將武器抽象化, 從外部傳入攻擊方式 (策略模式)
            avenger.equipWeapon(new Armor());
            avenger.Attack();

            Console.ReadLine();
        }
    }
}
