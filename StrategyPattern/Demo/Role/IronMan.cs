﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Demo
{
    public class IronMan
    {
        public void Talk()
        {
            Console.WriteLine("我是鋼鐵人！");
        }

        public void equipWeapon(EquipType equipType)
        {
            if(equipType == EquipType.Armor)
            {
                Armor armor = new Armor();
                armor.Attack();
            }
            else if(equipType == EquipType.Mjölnir)
            {
                Mjölnir mjölnir = new Mjölnir();
                mjölnir.Attack();
            }
            else if(equipType == EquipType.Shield)
            {
                Shield shield = new Shield();
                shield.Attack();
            }
        }
    }
}
