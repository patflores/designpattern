﻿using System;
using static AdaptorPattern.@enum;

namespace AdaptorPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            //阿仁的電腦的輸出訊號為HDMI
            PC Ren_PC = new PC(Signal.HDMI);

            //使用HDMI轉接器
            HDMIAdapter adapter = new HDMIAdapter(Ren_PC.Signal);

            //轉換訊號
            adapter.ConvertSignal();

            //使用投影機
            Projector projector = new Projector(adapter.Signal);
            projector.Display();

            Console.WriteLine("/**************************************/");

            //楷岳的電腦的輸出訊號為HDMI
            MacBook KaiYue_PC = new MacBook(Signal.MiniPort);

            //使用HDMI轉接器
            MiniPortAdapter adapter2 = new MiniPortAdapter(KaiYue_PC.Signal);

            //轉換訊號
            adapter2.ConvertSignal();

            //使用投影機
            Projector projector2 = new Projector(adapter2.Signal);
            projector2.Display();

            Console.ReadLine();
        }
    }
}
