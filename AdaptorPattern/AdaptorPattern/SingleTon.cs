﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdaptorPattern
{

    public class A
    {
        private static A instance;
        private A() { }

        public static A Instance
        {
            get
            {
                if (instance == null)
                    instance = new A();
                return instance;
            }
        }
    }

}
