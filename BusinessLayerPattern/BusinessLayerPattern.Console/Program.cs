﻿using BusinessLayerPattern.Service;
using System;

namespace BusinessLayerPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            var productService = new ProductService();
            var products = productService.getAllProducts();
        }
    }
}
