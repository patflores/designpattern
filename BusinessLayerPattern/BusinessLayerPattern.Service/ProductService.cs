﻿using BusinessLayerPattern.Models;
using BusinessLayerPattern.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLayerPattern.Service
{
    public class ProductService
    {
        private ProductRepository productRepository;

        public ProductService()
        {
            productRepository = new ProductRepository();
        }

        public IList<Product> getAllProducts()
        {
            IList<Product> products = productRepository.getAllProducts();
            return products;
        }
    }
}
