﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingletonPattern
{

    class Ticketer
    {
        // private constructor，這樣其他物件就無法用new來取得新的實體
        private Ticketer() { }

        private static Ticketer instance;

        //紀錄目前號碼
        private int currentNumber = 0;

        //提供一個Method, 讓其他程式調用這個類別
        public static Ticketer getInstance()
        {
            if (instance == null)
            {
                instance = new Ticketer();
            }
            return instance;
        }

        //取號的Method
        public int getNumber()
        {
            currentNumber++;
            return currentNumber;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Ticketer ticker = Ticketer.getInstance();

            System.Console.WriteLine("取號機1 目前的號碼是：" + ticker.getNumber());

            //取號機2取號
            Ticketer2();

            System.Console.WriteLine("取號機1 目前的號碼是：" + ticker.getNumber());
            System.Console.ReadLine();
        }

        static void Ticketer2()
        {
            Ticketer ticker = Ticketer.getInstance();
            System.Console.WriteLine("取號機2 目前的號碼是：" + ticker.getNumber());
        }
    }
}
