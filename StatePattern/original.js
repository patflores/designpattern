/**
 * 缺點
 * 1. 函數包含了許多if判斷，擴充不易
 * 2. 不遵守「開放/封閉」的設計原則。
 */

class trafficLight {

    constructor(state){
        this.state = state;
    }

    /**
     * 亮燈
     */
    light() {
        if (this.state == "RED") {
            console.log("紅燈");
        }
        else if (this.state == "GREEN") {
            console.log("綠燈");
        }
        else if (this.state == "YELLOW") {
            console.log("黃燈");
        }
        // else if (this.state == "BLUE"){
        //     console.log("藍燈");
        // }
        this.switchLight();
    };

    /**
     * 切換號誌
     */
    switchLight() {
        var self = this;
        let sec = 0;

        if (this.state == "RED") {
            this.state = "GREEN";
            sec = 5000;
        }
        else if (this.state == "GREEN") {
            this.state = "YELLOW";
            sec = 3000;
        }
        else if (this.state == "YELLOW") {
            this.state = "RED";
            sec = 1000;
        }
        // else if(this.state == "BULE"){
        //    ...
        // }

        console.log("等待 " + sec + "毫秒");

        setTimeout(function(){ self.light(); }, sec);
    };
}

var go_light = new trafficLight("RED");
go_light.light();