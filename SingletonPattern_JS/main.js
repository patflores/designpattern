/**
 * Created by 韻仁 on 2018/5/19.
 */

class Ticketer {

    // private constructor，這樣其他物件就無法用new來取得新的實體
    private Ticketer () { }

    private static instance = new Ticketer();
    private currentNumber = 0;

    //提供一個method, 讓其他程式調用這個類別
    public static getInstance() {
        return instance;
    }

    getNumber() {
        currentNumber++;
        return currentNumber;
    }
}


var ticker = Ticketer.getInstance();
console.log(ticker.getNumber());